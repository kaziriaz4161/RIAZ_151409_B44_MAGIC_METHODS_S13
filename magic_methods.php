<?php

class Person{

    public $name="Test Name";
    public $phone="01755555455";
    public $dateOfBirth = "01-01-1990";

    public static $message;

    public static function doFrequently(){

        echo "i'm doing it frequently... <br>";
    }

    public function doSomething(){

        echo "I'm doing something here... <br>";
    }

    public function __construct()
    {
        echo "I'm inside the ".__METHOD__."<br>";
    }

    public function __destruct()
    {
        echo "I'm dying, I'm inside the ".__METHOD__."<br>";
    }

    public function __call($name, $arguments)
    {
        echo "I'm inside the ".__METHOD__."<br>";

        echo "wrong name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";

    }


    public static function __callStatic($name, $arguments)
    {
        echo "I'm inside the static ".__METHOD__."<br>";

        echo "wrong static name = $name<br>";
        echo "<pre>";
        print_r($arguments);
        echo "</pre>";
    }


    public function __set($name, $value)
    {
        echo __METHOD__." Wrong Property name = $name<br>";
        echo "Value tried to set to that wrong property = $value<br>";
    }

    public function __get($name)
    {
        echo __METHOD__." Wrong Property name = $name<br>";
    }

    public function __isset($name)
    {
        echo __METHOD__." Wrong Property name = $name<br>";
    }


    public function __unset($name)
    {
        echo __METHOD__." Wrong Property name = $name<br>";
    }

    public function __sleep()
    {
        return array("name","phone");
    }

    public function __wakeup()
    {
        $this->doSomething();
    }



    public function __toString()
    {
        return "Are You Crazy? Don't forget i'm an Object.<br>";
    }

    public function __invoke()
    {
        echo "I'm an Object but you are trying to call me as a function! <br>";
    }

}// end of Person Class



Person::$message = "Any Message Here";   // static property setting test
echo Person::$message ."<br>";          // static property reading test

// wrong static method call
Person::doFrequentliiiiiii("wrong static para 1", "wrong static para 2"); //   __callStatic

$obj=  new Person();  // __construct

$obj->doSomething();  // user defined method call

$obj->habijabi("para_habi1","para_jabi1");   //  __call()

$obj->address ="5/2 Gol Pahar Mor, Chittagong";   // __set()

echo $obj->address;        // __get()


if( isset ($obj->naiProperty)   ){    // __isset()

}

unset ($obj->naiProperty);      //  __unset()



$myVar = serialize($obj);
echo "<pre>";
var_dump($myVar);
echo "</pre>" ;

echo "<pre>";
var_dump( unserialize($myVar) );
echo "</pre>" ;


echo $obj;  // __toString


$obj();
